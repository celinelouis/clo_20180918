'use strict';
const REST_SERVER_ADDRESS = 'http://localhost:5000/notes/';

var crudJson = new CRUD(REST_SERVER_ADDRESS, 'notes');

function isLoaded() {
    console.log('code JS pas chargé');
    var maBalise = document.getElementById('status');
    maBalise.innerText = "code JS chargé";
    maBalise.style.backgroundColor = 'GREEN';
    console.log('code JS chargé');

    addEventsForNoteForm();

    var parametresReadAll = {
        callback: function(notes) {
            var list = document.querySelector('#list-note');
            notes.forEach(function(uneNoteDeLarray) {
                addNote(list, uneNoteDeLarray);
            });
        }
    };

    crudJson.read(parametresReadAll);

    //loadNotes();
}

function addEventsForNoteForm() {
    console.log('add events for note form');

    //version 2 par query selector
    var button = document.querySelector('button#saveButton');
    button.addEventListener('click', saveNote);

    //sélection du bouton dont l'id est resetButton
    //on recycle la variable button
    button = document.querySelector('button#resetButton');
    button.addEventListener('click', resetNote);

    var color = document.querySelector('#color-note').value;
    console.log(color);
}


function saveNote() {
    var notesData = getFormValues();
    if (undefined !== notesData.id) {
        //id existant, c'est un update
        var paramsUpdate = {
            postElement: getFormValues(),
            callback: function(note) {
                var listNote = document.querySelector('#list-note');

                var newNote = createNodeNote(note);
                var oldNote = document.querySelector('#note-' + note.id);
                listNote.replaceChild(newNote, oldNote);
                resetNote();
            }
        }
        crudJson.updateNote(paramsUpdate);
        console.log('save note by event');
    } else {
        //id pas défini, c'est un create
        var paramsCreate = {
            postElement: getFormValues(),
            callback: function(note) {
                var listNote = document.querySelector('#list-note');
                addNote(listNote, note);
                resetNote();
            }
        }
        crudJson.create(paramsCreate);
        console.log('save note by event');
    }
}

function resetNote() {

    document.forms.noteForm.reset();

    console.log('reset note by event');
}

function createNodeNote(note) {
    var noteDiv = document.createElement('div');
    noteDiv.setAttribute('class', 'vue-note');
    noteDiv.setAttribute('id', 'note-' + note.id);
    // a cet endroit on peut toucher au innerHtml car cela n'a pas d'impact, il n'existe pas encore
    noteDiv.innerHTML = ' <div class="close-icon-block"><img src="img/delete.svg" /></div>' + note.nom;
    noteDiv.querySelector('.close-icon-block>img').addEventListener('click', removeNote);
    noteDiv.addEventListener('click', function(evt) {
        console.log('je viens de cliquer sur une note');
        crudJson.read({
            id: note.id,
            callback: putNoteInForm
        })
    });
    return noteDiv;
}

function addNote(listNote, note) {
    var noteToAdd = createNodeNote(note);
    listNote.appendChild(noteToAdd);
}


function updateNote(listNote, note) {
    var noteDiv = document.querySelector('note-' + note.id);

}

function putNoteInForm(note) {
    console.log(note);
    var form = document.forms.noteForm;
    form.querySelector('#id-note').value = note.id;
    form.querySelector('#nom-note').value = note.nom;
    form.querySelector('#date-note').value = note.date;
    form.querySelector('#heure-note').value = note.heure;
    form.querySelector('#description-note').value = note.description;
}

function updateNote(note, id) {
    var paramsUpdate = {
        postElement: getFormValues(),
        callback: function(note) {
            var listNote = document.querySelector('#list-note');
            addNote(listNote, note)
        }
    }

}

function getFormValues() {
    var monForm = document.forms.noteForm;
    // toutes les fonctions retournent un objet
    var returnObject = {};
    returnObject.date = monForm.querySelector('#date-note').value;
    returnObject.heure = monForm.querySelector('#heure-note').value;
    returnObject.description = monForm.querySelector('#description-note').value;
    returnObject.nom = monForm.querySelector('#nom-note').value;
    var idNote = monForm.querySelector('#id-note').value;
    if (idNote != '') {
        returnObject.id = idNote;
    }

    console.log(returnObject);

    return returnObject;
}

function removeNote(evt) {
    console.log('je suis dans le remove note');
    //arrête la propagation des event pour ne pas passer dansl'evenement de la div de la note 
    evt.stopPropagation();
    console.log(evt);

    var idNote = evt.target.parentElement.parentElement.id;
    if (!confirm('Confirmer la suppression de la note :' + idNote)) return undefined;

    var array = idNote.split('-');
    console.log(array);
    idNote = array[1];
    console.log("id note:" + idNote);

    var paramsDelete = {
        id: idNote,
        callback: function() {
            evt.target.parentElement.parentElement.remove()
        }


    }
    crudJson.deleteNote(paramsDelete);
}

isLoaded();