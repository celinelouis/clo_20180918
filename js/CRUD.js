'use strict';

function CRUD(url, ressourceName) {

    //déclaration des propriétés privées
    this.lastResult = null;

    //déclaration d'une référence accessible dans les scopes des fonctions déclarées à la volée
    //var _vm = this;

    //déclaration des fonctions
    this.create = function(params) {

        var uri = url;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = (function(response) {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status < 300) {
                var maNote = JSON.parse(xhr.response);
                //à ce niveau, this est une XmlHttpRequest
                console.log(this);
                //du coup si on veut garder le dernier résultat on passe par notre variable _vm
                //console.log(_vm);
                this.lastResult = maNote;
                if (undefined !== params.callback) params.callback(maNote);
            }
            //on binde un objet, this en l'occurence mais ca pourrait etre  
        }).bind(this);
        xhr.send(JSON.stringify(params.postElement));
    }

    //on passe un objet params qui est générique
    //s'il contient la property id on lit uniquement la note avec cet id
    //sinon on lit toutes les notes

    this.read = function(params) {
        var xhr = new XMLHttpRequest();

        //on contruit l'URI avec l'id ou pas si on veut tout
        var uri = url + ((undefined !== params.id) ? params.id : '');
        xhr.open('GET', uri, true);
        xhr.onreadystatechange = function(response) {
            ///   console.log(this);
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status < 300) {
                var mesNotes = JSON.parse(xhr.response);
                if (undefined !== params.callback) params.callback(mesNotes);
            }
        };
        xhr.send();
    }

    this.deleteNote = function(params) {
        var xhr = new XMLHttpRequest();
        var uri = url + params.id;
        xhr.open('DELETE', uri, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function(response) {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status < 300) {
                var maNote = JSON.parse(xhr.response);
                if (undefined !== params.callback) params.callback(maNote);
            }
        };
        xhr.send();
    }

    this.updateNote = function(params) {

        var uri = url + params.postElement.id;
        var xhr = new XMLHttpRequest();
        xhr.open('PUT', uri, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = (function(response) {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status < 300) {
                var maNote = JSON.parse(xhr.response);
                this.lastResult = maNote;
                if (undefined !== params.callback) params.callback(maNote);
            }
        }).bind(this);
        xhr.send(JSON.stringify(params.postElement));
    }
}